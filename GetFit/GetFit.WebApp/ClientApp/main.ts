import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);
 
// It's for reloading application while new changes is added
if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => {
        // Before reload creates new "app" element, and destroys old "app" element
        const oldRootElem = document.querySelector('app');
        const newRootElem = document.createElement('app');
        oldRootElem!.parentNode!.insertBefore(newRootElem, oldRootElem);
        platform.destroy();
    });
}