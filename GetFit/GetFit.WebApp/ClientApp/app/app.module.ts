import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // added animations for correct work of primeng elements
import { ButtonModule, FieldsetModule, PasswordModule, InputTextModule } from 'primeng/primeng'; // Primeng modules

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HttpClientModule } from '@angular/common/http';
import { NavigationProvider } from './services/navmenu.service';
import { HomeComponent } from './components/home/home.component';

@NgModule({
    imports: [BrowserModule, FormsModule, CommonModule, BrowserAnimationsModule,
        ButtonModule, FieldsetModule, PasswordModule, InputTextModule,  // Added Primeng modules
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    declarations: [AppComponent, NavMenuComponent, HomeComponent],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }