﻿import { Component } from '@angular/core';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.scss',
        '../../../../node_modules/font-awesome/css/font-awesome.min.css', // lib with necessary icons for primeg UI elements
        '../../../../node_modules/primeng/resources/themes/omega/theme.css', // primeng theme 
        '../../../../node_modules/primeng/resources/primeng.min.css'  // primeng styles
    ]

})
export class AppComponent {

}