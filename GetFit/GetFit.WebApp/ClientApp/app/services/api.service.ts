﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class ApiService {
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {

    }
    private setHeaders(): Headers {
        const headersConfig = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        let headers = new Headers(headersConfig);
        headers.append('Access-Control-Allow-Origin', 'http://localhost:44375');
        return new Headers(headersConfig);
    }
    private formatErrors(error: any) {
        if (error.status === 401) {
            window.location.replace('/account/login');
        }
        if (error.status === 403) {
            window.location.replace('/home');
        }
        return Observable.throw(error.json());
    }
    get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
        return this.http.get(`${this.baseUrl}${path}`, { headers: this.setHeaders(), search: params })
            .catch(this.formatErrors)
            .map((res: Response) => res.json());
    }

    put(path: string, body: Object = {}): Observable<any> {
        return this.http.put(`${this.baseUrl}${path}`, JSON.stringify(body), { headers: this.setHeaders() })
            .catch(this.formatErrors)
            .map((res: Response) => res.json());
    }

    post(path: string, body: Object = {}): Observable<any> {
        return this.http.post(`${this.baseUrl}${path}`, JSON.stringify(body), { headers: this.setHeaders() })
            .catch(this.formatErrors)
            .map((res: Response) => res.json());
    }

    delete(path: string): Observable<any> {
        return this.http.delete(`${this.baseUrl}${path}`, { headers: this.setHeaders() })
            .catch(this.formatErrors)
            .map((res: Response) => res.json());
    }
}


