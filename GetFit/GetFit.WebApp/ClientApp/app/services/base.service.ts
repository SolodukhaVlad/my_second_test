﻿import { BaseModel } from "../models/baseModel";
import { ApiService } from './api.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

export abstract class BaseService<TModel extends BaseModel> {
    protected constructor(
        protected apiService: ApiService,
      
        
        protected sourceLink: string,
    ) {
    }
    getSourseLink(): string {
        return this.sourceLink;
    }
    getAll(): Promise<TModel[]> {
        return new Promise((resolve, reject) => {
            this.apiService.get(`api/${this.sourceLink}`)
                .subscribe(data => {
                    if (data.wasSuccessful) {
                        resolve(data.records);
                    } else {
                        reject();
                    }
                }, error => {
                    reject();
                });
        });
    }
    saveRecord(record: TModel): Promise<TModel> {
        return new Promise((resolve, reject) => {
            if (record.id > 0) {
                this.apiService.put(`api/${this.sourceLink}/` + record.id, record)
                    .subscribe(data => {
                        if (data.wasSuccessful) {
                            resolve(data.record);
                        } else {
                            reject();
                        }

                    }, error => {
                        reject();
                    });
            }
            else {
                this.apiService.post(`api/${this.sourceLink}`, record)
                    .subscribe(data => {
                        if (data.wasSuccessful) {
                            resolve(data.record);
                        } else {
                            reject();
                        }
                    }, error => {
                        reject();
                    });
            }
        });
    }
    getRecordById(id: number): Promise<TModel> {
        return new Promise((resolve, reject) => {
            this.apiService.get(`api/${this.sourceLink}/GetSingleObject?id=${id}`)
                .subscribe(data => {
                    if (data.wasSuccessful) {
                        resolve(data.record);
                    } else {
                        reject();
                    }
                }, error => {
                    
                    reject();
                });
        });
    }
    deleteRecordById(id: number): Promise<TModel> {
       
        return new Promise((resolve, reject) => {
            this.apiService.delete(`api/${this.sourceLink}/` + id)
                .subscribe(data => {
                   
                    if (data.wasSuccessful) {
                        resolve(data.record);
                       
                    } else {
                        
                        reject();
                    }
                }, error => {
                   
                    reject();
                });
        });
    }
}