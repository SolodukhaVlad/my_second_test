﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class NavigationProvider {

    private static navigations: any[] = [];

    constructor() { }

    getNavigations(): any[] {
        return NavigationProvider.navigations;
    }


    setNavigations(navigations: any[]) {
        NavigationProvider.navigations = navigations;
    }
}