﻿import { BaseModel } from './baseModel';

export class TrainerRegModel extends BaseModel {
    
        public id: number;
        public firstName: string;
        public lastName: string;
        public dayOfBirth: string;
        public email: string;
        public password: string;
        public photoUrl?: string;
        public phoneNumber?: string;
        public streetName?: string;
        public houseNumber?: string;
        public city?: string;
    
}