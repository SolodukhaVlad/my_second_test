﻿using System.IO;
using GetFit.BusinessLogic.Services.Interfaces;
using GetFit.DataAccess;
using GetFit.DataAccess.ContextOptionsProvider;
using GetFit.DataAccess.Interfaces;
using GetFit.BusinessLogic.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

namespace Web
{
	public class Startup
	{
		/// <summary>
		/// Create Startup
		/// </summary>
		/// <param name="env">Provides information about the web hosting environment an application is running</param>
		public Startup(IHostingEnvironment env)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", false, true);
			builder.AddEnvironmentVariables();
			Configuration = builder.Build();
		}

		/// <summary>
		/// Represents a set of key/value application configuration properties
		/// </summary>
		public IConfiguration Configuration { get; }

		/// <summary>
		/// This method gets called by the runtime. Use this method to add services to the container.
		/// </summary>
		/// <param name="services">Container to register services</param>
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc();
			services.AddSingleton(Configuration);
			services.AddSingleton<IDbContextOptionsProvider, DefaultContextOptionsProvider>();
			services.AddDbContext<GetFitContext>();
			services.AddTransient<ICodeTableServicesFactory, CodeTableServicesFactory>();
		}

		/// <summary>
		/// This method gets called by the runtime. Use this method to configure the HTTP request pipeline
		/// </summary>
		/// <param name="app">Object that provides the mechanisms to configure an application's request</param>
		/// <param name="env">Provides information about the web hosting environment an application is running</param>
		/// <param name="loggerFactory">Object that provides loger</param>
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
				{
					HotModuleReplacement = true
				});
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			app.UseStaticFiles(new StaticFileOptions
			{
				FileProvider = new PhysicalFileProvider(
					Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),
					RequestPath = "/wwwroot"
			});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                routes.MapSpaFallbackRoute("angular-fallback",
                    new { controller = "Home", action = "Index" });
            });
            loggerFactory.AddConsole();
		}
	}
}
