var path = require('path');
var webpack = require('webpack');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin'); // plugin for minifying
const isDevBuild = !process.env.production;

module.exports = {
    entry: {
        'polyfills': './ClientApp/polyfills.ts',
        'app': './ClientApp/main.ts'
    },
    output: {
        path: path.resolve(__dirname, './wwwroot/dist'),
        publicPath: './wwwroot/dist/',
        filename: "[name].js"
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/, 
                use: isDevBuild ? [
                    {
                        loader: 'awesome-typescript-loader',
                        options: { configFileName: path.resolve(__dirname, 'tsconfig.json') }
                    },
                    'angular2-template-loader'
                ] : '@ngtools/webpack'
            },{
              test: /\.html$/,
              loader: 'html-loader'
            },
            {
                test: /\.css$/,
                use: ['to-string-loader', 'style-loader', isDevBuild ? 'css-loader' : 'css-loader?minimize']
            },
            {
                test: /\.scss$/,
                loader: ['to-string-loader', 'style-loader', isDevBuild ? 'css-loader' : 'css-loader?minimize', 'sass-loader']
            },
            { test: /\.(png|woff|woff2|eot|ttf|ico|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core/,
            path.resolve(__dirname, 'ClientApp'),
            {}
        ),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'polyfills']
        }),
        new UglifyJSPlugin()
    ]
}
