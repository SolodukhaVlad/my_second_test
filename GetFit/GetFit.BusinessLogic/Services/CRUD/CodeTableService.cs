﻿using System.Collections.Generic;
using System.Linq;
using GetFit.DataAccess.Interfaces;
using GetFit.DataAccess.Repository;
using GetFit.BusinessLogic.Interfaces;
using GetFit.BusinessLogic.Models;
using GetFit.BusinessLogic.Services.CRUD.Core;
using GetFit.BusinessLogic.Services.Interfaces;

namespace GetFit.BusinessLogic.Services.CRUD
{
	/// <summary>
	/// Service to get code tables from data base using entity type
	/// this code table need to have Code and Description
	/// </summary>
	/// <typeparam name="TCodeTableEntity"></typeparam>
	public class CodeTableService<TCodeTableEntity> : ICodeTableService
		where TCodeTableEntity : class, ICodeTableEntity, new()
	{
		#region Data Members

		private readonly DataRepository<TCodeTableEntity> _dataRepository;

		#endregion

		#region Constructors

		public CodeTableService(IDbContextOptionsProvider optionsProvider)
		{
			_dataRepository = new DataRepository<TCodeTableEntity>(optionsProvider);
		}

		#endregion

		#region ICodeTableService Members

		public ICodeTableModel GetSingleByDesc(string desc)
		{
			var entity = _dataRepository.GetSingle(i => i.Description == desc);
			return new CodeTableModel<TCodeTableEntity>().FromEntity(entity) as ICodeTableModel;
		}

		/// <summary>
		/// Create code table service
		/// </summary>s
		public CollectionOperationResult<ICodeTableModel> GetRecords()
		{
			var entities = _dataRepository.Get().ToList();

			var collection = new List<ICodeTableModel>();
			foreach (var entity in entities)
			{
				var model = new CodeTableModel<TCodeTableEntity>().FromEntity(entity);
				collection.Add(model as ICodeTableModel);
			}
			return new CollectionOperationResult<ICodeTableModel>(OperationTypes.Read, true, "Code table loaded", collection);
		}

		#endregion
	}

}
