﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;
using GetFit.BusinessLogic.Models.Helper;
using GetFit.BusinessLogic.Validation;
using GetFit.BusinessLogic.Services.Interfaces.Core;

namespace GetFit.BusinessLogic.Services.CRUD.Core
{
	///<summary>
	/// Base service class, represents basic functionality to work with models entities and data repositories.
	/// for add, update, and delete data.
	/// </summary>
	/// <typeparam name="TModel">Model for operation which inherit BaseModel</typeparam>
	/// <typeparam name="TEntity">Entity object for operation which inherit BaseEntity</typeparam>
	public abstract class ReadWriteDataService<TModel, TEntity> : ReadDataService<TModel, TEntity>, IWriteDataService<TModel>
		where TModel : class, IBaseModel<TEntity>, new()
		where TEntity : class, IBaseEntity, new()
	{
		#region Members

		protected IModelValidator<TModel> ModelValidator = new DefaultModelValidator<TModel>();

		#endregion

		#region Constructors

		/// <summary>
		/// Creates ReadWrite service.
		/// </summary>
		/// <param name="optionsProvider">Object which is providing db context options</param>
		/// <param name="logger">Object used to log events.</param>
		protected ReadWriteDataService(IDbContextOptionsProvider optionsProvider, ILogger logger)
			: base(optionsProvider, logger)
		{
		}

		#endregion

		#region Methods for overrides

		/// <summary>
		/// Method to convert entity before commit into database
		/// </summary>
		/// <param name="operation">Operation type.</param>
		/// <param name="model">Model to be converted.</param>
		/// <returns>Current model to check data.</returns>
		protected virtual TEntity ConvertModelToEntity(OperationTypes operation, TModel model)
		{
			return model.ToEntity();
		}

		protected virtual SingleOperationResult<TModel> PerformSingleOperation(OperationTypes operationType, TModel model, Func<TEntity, TEntity> operationFunc)
		{
			var errorMessageByLevel = string.Empty;
			try
			{
				var validationErrors = ModelValidator.Validate(model);
				if (validationErrors.Any())
				{
					return SingleError(operationType, model, validationErrors);
				}
				errorMessageByLevel = ServerMessages.ModelConversionError;
				var entity = ConvertModelToEntity(operationType, model);
				errorMessageByLevel = ServerMessages.ErrorInDataBase;
				operationFunc(entity);
				errorMessageByLevel = ServerMessages.ErrorWhileRetrieving;
				entity = DataRepository.GetSingle(i => i.Id == entity.Id, IncludePropertyFunc);
				return SingleSuccess(operationType, entity.ToModel<TModel, TEntity>(), GetSingleOperationMessage);
			}
			catch (Exception ex)
			{
				LogException(ex);
				var result = SingleError(operationType, model, GetSingleOperationMessage);
				result.Message += $" {errorMessageByLevel}.";
#if DEBUG
				result.Message += $" Exception: {ex.Message} StackTrace: {ex.StackTrace}";
#endif
				return result;
			}
		}

		protected virtual CollectionOperationResult<TModel> PerformCollectionOperation(OperationTypes operationType, IEnumerable<TModel> models, Action<IEnumerable<TEntity>> operationFunc)
		{
			try
			{
				var validationErrors = models.SelectMany(model => ModelValidator.Validate(model));
				if (validationErrors.Any())
				{
					return CollectionError(OperationTypes.Add, models, validationErrors);
				}
				var entities = models.Select(model => ConvertModelToEntity(OperationTypes.Add, model));
				operationFunc(entities);
				return CollectionSuccess(OperationTypes.Add, entities.ToModels<TModel, TEntity>(), GetCollectionOperationMessage);
			}
			catch (Exception ex)
			{
				LogException(ex);
				return CollectionError(OperationTypes.Add, models, GetCollectionOperationMessage);
			}
		}

		private string GetSingleOperationMessage(OperationTypes operation, TModel model, bool isSuccess = true)
		{
			var message = $"{model.GetFriendlyName()} was {(isSuccess ? "" : "not")}";
			switch (operation)
			{
				case OperationTypes.Add: message = $"{message} added."; break;
				case OperationTypes.Update: message = $"{message} updated."; break;
				case OperationTypes.Delete: message = $"{message} deleted."; break;
			}
			return isSuccess ? message : $"{message} {ServerMessages.TryAgain}.";
		}

		private string GetCollectionOperationMessage(OperationTypes operation, IEnumerable<TModel> models, bool isSuccess = true)
		{
			var message = $"Records was {(isSuccess ? "" : "not")}";
			switch (operation)
			{
				case OperationTypes.Add: message = $"{message} added."; break;
				case OperationTypes.Update: message = $"{message} updated."; break;
				case OperationTypes.Delete: message = $"{message} deleted."; break;
			}
			return isSuccess ? message : $"{message} {ServerMessages.TryAgain}.";
		}

		#endregion

		#region IWriteDataService<TModel,TEntity> implementation

		public virtual SingleOperationResult<TModel> Add(TModel model)
		{
			return PerformSingleOperation(OperationTypes.Add, model, DataRepository.Add);
		}

		public virtual CollectionOperationResult<TModel> AddRange(IEnumerable<TModel> models)
		{
			return PerformCollectionOperation(OperationTypes.Add, models, DataRepository.AddRange);
		}

		public virtual SingleOperationResult<TModel> Update(TModel model)
		{
			return PerformSingleOperation(OperationTypes.Update, model, DataRepository.Update);
		}

		public virtual CollectionOperationResult<TModel> UpdateRange(IEnumerable<TModel> models)
		{
			return PerformCollectionOperation(OperationTypes.Update, models, DataRepository.Update);
		}

		public virtual SingleOperationResult<TModel> Remove(int id)
		{
			var entity = DataRepository.GetSingle(i => i.Id == id, IncludePropertyFunc);

			try
			{
				var result = SingleSuccess(OperationTypes.Delete, entity.ToModel<TModel, TEntity>(), GetSingleOperationMessage);
				DataRepository.Remove(id);
				return result;
			}
			catch (Exception ex)
			{
				LogException(ex);
				return SingleSuccess(OperationTypes.Delete, entity.ToModel<TModel, TEntity>(), GetSingleOperationMessage);
			}
		}

		public virtual SingleOperationResult<TModel> SaveChanges(TModel model, OperationTypes operation)
		{
			try
			{
				switch (operation)
				{
					case OperationTypes.Add:
						return Add(model);
					case OperationTypes.Update:
						return Update(model);
					case OperationTypes.Delete:
						return Remove(model.Id);
					default:
						return SingleError(operation, ServerMessages.UnsupportedOperation, model);
				}
			}
			catch (Exception ex)
			{
				LogException(ex);
				return SingleError(operation, model);
			}
		}

		public virtual IEnumerable<SingleOperationResult<TModel>> SaveChanges(IEnumerable<TModel> models, OperationTypes operation)
		{
			return models.Select(m => SaveChanges(m, operation));
		}

		#endregion
	}
}
