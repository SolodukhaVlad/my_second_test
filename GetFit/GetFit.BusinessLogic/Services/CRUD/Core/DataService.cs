﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Linq;
using Microsoft.Extensions.Logging;
using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;
using GetFit.BusinessLogic.Services.Interfaces.Core;

namespace GetFit.BusinessLogic.Services.CRUD.Core
{
	public class DataService<TModel, TEntity> : IDataService
		where TModel : class, IBaseModel<TEntity>, new()
		where TEntity : class, IBaseEntity, new()
	{

		#region Constructors

		/// <summary>
		/// Creates ReadOnly service and set custom data repository.
		/// </summary>
		/// <param name="dataRepository">Custom data repository.</param>
		/// <param name="logger">Object used to log events.</param>
		protected DataService(IDataRepository<TEntity> dataRepository, ILogger logger)
		{
			DataRepository = dataRepository;
			Logger = logger;
		}

		#endregion

		#region Members

		/// <summary>
		/// Repository to work with database
		/// </summary>
		protected IDataRepository<TEntity> DataRepository;

		/// <summary>
		/// Object used to log events.
		/// </summary>
		protected ILogger Logger;

		#endregion

		#region Methods

		/// <summary>
		/// Log exception using default logger
		/// </summary>
		/// <param name="ex">Exception object</param>
		/// <param name="memberName">Caller member name</param>
		protected void LogException(Exception ex, [CallerMemberName]string memberName = "")
		{
			Logger.LogError("{0} - {1} \n {2}", memberName, ex.Message, ex.StackTrace);
		}

		/// <summary>
		/// Get operation result with error and custom message for single record
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="customMessage">Custom error message</param>
		/// <param name="model">Model that has been used for operation</param>
		/// <returns>Operation result (Single/Error)</returns>
		protected SingleOperationResult<TModel> SingleError(OperationTypes operation, string customMessage, TModel model = default(TModel))
		{
			return new SingleOperationResult<TModel>(operation, false, customMessage, model);
		}

		/// <summary>
		/// Get default operation result with error for single record
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="model">Model that has been used for operation</param>
		/// <returns>Operation result (Single/Error)</returns>
		protected SingleOperationResult<TModel> SingleError(OperationTypes operation, TModel model = null)
		{
			return new SingleOperationResult<TModel>(operation, false, ServerMessages.CannotPerformOperation, model);
		}

		/// <summary>
		/// Get operation result with validation errors for single record
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="model">Model that has been used for operation</param>
		/// <param name="validatinResults">Results of validation</param>
		/// <returns>Operation result (Single/Error)</returns>
		protected SingleOperationResult<TModel> SingleError(OperationTypes operation, TModel model, IEnumerable<ValidationResult> validatinResults)
		{
			return new SingleOperationResult<TModel>(operation, false, validatinResults.ToString(), model);
		}

		/// <summary>
		/// Get operation result with success and custom message for single record
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="customMessage">Custom error message</param>
		/// <param name="model">Model that has been used for operation</param>
		/// <returns>Operation result (Single/Error)</returns>
		protected SingleOperationResult<TModel> SingleSuccess(OperationTypes operation, string customMessage, TModel model = default(TModel))
		{
			return new SingleOperationResult<TModel>(operation, true, customMessage, model);
		}

		/// <summary>
		/// Get default successful operation result for single record
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="model">Model that has been used for operation</param>
		/// <returns>Operation result (Single/Success)</returns>
		protected SingleOperationResult<TModel> SingleSuccess(OperationTypes operation, TModel model = null)
		{
			return new SingleOperationResult<TModel>(operation, true, ServerMessages.OperationCompleated, model);
		}

		protected SingleOperationResult<TModel> SingleSuccess(OperationTypes operation, TModel model, Func<OperationTypes,TModel,bool,string> retrieveMessageFunc)
		{
			var message = retrieveMessageFunc(operation, model, true);
			return new SingleOperationResult<TModel>(operation, true, message, model);
		}

		protected SingleOperationResult<TModel> SingleError(OperationTypes operation, TModel model, Func<OperationTypes, TModel, bool, string> retrieveMessageFunc)
		{
			var message = retrieveMessageFunc(operation, model, false);
			return new SingleOperationResult<TModel>(operation, false, message, model);
		}

		protected CollectionOperationResult<TModel> CollectionError(OperationTypes operation, IEnumerable<TModel> models, Func<OperationTypes, IEnumerable<TModel>, bool, string> retrieveMessageFunc)
		{
			var message = retrieveMessageFunc(operation, models, false);
			return new CollectionOperationResult<TModel>(operation, false, message, models.ToList());
		}

		protected CollectionOperationResult<TModel> CollectionSuccess(OperationTypes operation, IEnumerable<TModel> models, Func<OperationTypes, IEnumerable<TModel>, bool, string> retrieveMessageFunc)
		{
			var message = retrieveMessageFunc(operation, models, true);
			return new CollectionOperationResult<TModel>(operation, true, message, models.ToList());
		}

		/// <summary>
		/// Get operation result with custom messaga for collection
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="message">Custom message</param>
		/// <returns>Operation result (Collection/Error)</returns>
		protected CollectionOperationResult<TModel> CollectionError(OperationTypes operation, string message)
		{
			return new CollectionOperationResult<TModel>(operation, false, message);
		}

		/// <summary>
		/// Get operation result with validation errors for colection of records
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="models">Collection of models that have been used for operation</param>
		/// <param name="validatinResults">Collection of validation results</param>
		/// <returns>Operation result (Collection/Error)</returns>
		protected CollectionOperationResult<TModel> CollectionError(OperationTypes operation, IEnumerable<TModel> models, IEnumerable<ValidationResult> validatinResults)
		{
			return new CollectionOperationResult<TModel>(operation, false, validatinResults.ToString());
		}

		/// <summary>
		/// Get default operation result with errors for collection
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="models">Collection of models that have been used for operation</param>
		/// <returns>Operation result (Collection/Error)</returns>
		protected CollectionOperationResult<TModel> CollectionError(OperationTypes operation, IEnumerable<TModel> models)
		{
			var message = string.Empty;
			foreach (var model in models)
			{
				message += string.Format("{0} ", SingleError(operation, model));
			}
			return new CollectionOperationResult<TModel>(operation, false, message, models.ToList());
		}

		/// <summary>
		/// Get successful operation result for collection
		/// </summary>
		/// <param name="operation">Operation Type</param>
		/// <param name="models">Collection of models that have been used for operation</param>
		/// <returns>Operation result (Collection/Success)</returns>
		protected CollectionOperationResult<TModel> CollectionSuccess(OperationTypes operation, IEnumerable<TModel> models)
		{
			return new CollectionOperationResult<TModel>(operation, true, ServerMessages.DataDownloaded, models.ToList());
		}

		#endregion
	}
}
