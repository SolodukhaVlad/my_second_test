﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.BusinessLogic.Models;
using GetFit.DataAccess.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace GetFit.BusinessLogic.Services.CRUD.Core
{
    public class UserReadWriteDataService<TModel, TEntity> : ReadWriteDataService<TModel, TEntity>
        where TModel : class, IBaseModel<TEntity>, new()
        where TEntity : class, IBaseEntity, new()
    {

        public UserReadWriteDataService(IDbContextOptionsProvider optionsProvider, ILogger logger)
            : base(optionsProvider, logger)
        {

        }

        public ICollection<SingleOperationResult<TModel>> RegisterUser(IUserRegModel regModel)
        {            
            var results = new HashSet<SingleOperationResult<TModel>>();
            SingleOperationResult<TModel> res;
            if (regModel.AddressModel != null)
            {
                res = Add(regModel.AddressModel as TModel);
                results.Add(res);
                regModel.UserModel.AddressId = res.Record.Id;
            }

            if(regModel.ContactModel != null)
            {
                res = Add(regModel.ContactModel as TModel);
                results.Add(res);
                regModel.UserModel.ContactId = res.Record.Id;
            }

            res = Add(regModel.UserModel as TModel);
            results.Add(res);

            return results;
        }
    }
}
