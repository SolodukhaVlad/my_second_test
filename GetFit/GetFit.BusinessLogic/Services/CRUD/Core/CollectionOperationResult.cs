﻿using System.Collections.Generic;
using GetFit.BusinessLogic.Interfaces;

namespace GetFit.BusinessLogic.Services.CRUD.Core
{
	/// <summary>
	/// Incapsulates result of operation for list of objects.
	/// </summary>
	/// <typeparam name="TModel">Type of value returned by operation.</typeparam>
	public class CollectionOperationResult<TModel> : OperationResult where TModel : IBaseModel
	{
		public CollectionOperationResult(OperationTypes operationType, bool wasSuccessful, string message, List<TModel> models)
			: base(operationType, wasSuccessful, message)
		{
			Records = models;
		}

		public CollectionOperationResult(OperationTypes operationType, bool wasSuccessful, string message)
			: base(operationType, wasSuccessful, message)
		{
		}

		/// <summary>
		/// Resulting value.
		/// </summary>
		public List<TModel> Records { get; set; }
	}
}
