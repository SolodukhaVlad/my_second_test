﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;
using GetFit.DataAccess.Repository;
using GetFit.BusinessLogic.Models.Helper;
using GetFit.BusinessLogic.Services.Interfaces.Core;

namespace GetFit.BusinessLogic.Services.CRUD.Core
{
	/// <summary>
	/// Base read only service class, represents basic functionality to work with models and model validation, entities, data repositories.
	/// </summary>
	/// <typeparam name="TModel">Model with which service works</typeparam>
	/// <typeparam name="TEntity">Entity with which service works</typeparam>
	public abstract class ReadDataService<TModel, TEntity> : DataService<TModel, TEntity>, IReadDataService<TModel, TEntity>
		where TModel : class, IBaseModel<TEntity>, new()
		where TEntity : class, IBaseEntity, new()
	{
		#region Data Members

		protected readonly Func<IQueryable<TEntity>, IQueryable<TEntity>> IncludePropertyFunc;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates ReadOnly service and set custom data repository.
		/// </summary>
		/// <param name="optionsProvider">Object which is providing db context options</param>
		/// <param name="logger">Object used to log events.</param>
		protected ReadDataService(IDbContextOptionsProvider optionsProvider, ILogger logger)
			: this(new DataRepository<TEntity>(optionsProvider), logger)
		{
		}

		/// <summary>
		/// Creates ReadOnly service and set custom data repository.
		/// </summary>
		/// <param name="dataRepository">Custom data repository.</param>
		/// <param name="logger">Object used to log events.</param>
		protected ReadDataService(IDataRepository<TEntity> dataRepository, ILogger logger)
			: base(dataRepository, logger)
		{
			IncludePropertyFunc = includeQuery => new TModel().IncludeProperties(includeQuery);
		}

		#endregion

		#region IReadDataService Implementation

		public virtual SingleOperationResult<TModel> GetSingleRecord(int id)
		{
			return GetSingleRecord(e => e.Id == id);
		}

		public SingleOperationResult<TModel> GetSingleRecord(Expression<Func<TEntity, bool>> predicate)
		{
			try
			{
				var model = DataRepository
					.GetSingle(predicate, IncludePropertyFunc)
					.ToModel<TModel, TEntity>();

				return SingleSuccess(OperationTypes.Read, model);
			}
			catch (Exception ex)
			{
				LogException(ex);
				return SingleError(OperationTypes.Read);
			}
		}

		public virtual CollectionOperationResult<TModel> GetRecords()
		{
			return GetRecords(null);
		}

		public virtual CollectionOperationResult<TModel> GetRecords(Expression<Func<TEntity, bool>> predicate)
		{
			try
			{
				var records = DataRepository
					.Get(predicate, IncludePropertyFunc)
					.ToModels<TModel, TEntity>();
				return CollectionSuccess(OperationTypes.Read, records.ToList());
			}
			catch (Exception ex)
			{
				LogException(ex);
				return CollectionError(OperationTypes.Read, ServerMessages.CannotLoadRecords);
			}
		}

		#endregion
	}
}
