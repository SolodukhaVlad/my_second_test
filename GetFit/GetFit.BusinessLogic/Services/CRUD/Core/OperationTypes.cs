﻿namespace GetFit.BusinessLogic.Services.CRUD.Core
{
	/// <summary>
	/// Enums for check operation type. using in sevices
	/// for add, delete and update data
	/// </summary>
	public enum OperationTypes
	{
		Add,
		Update,
		Delete,
		AddAndUpdate,
		Read
	}
}
