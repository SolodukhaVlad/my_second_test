﻿using System;

namespace GetFit.BusinessLogic.Services.Interfaces.Core
{
	/// <summary>
	/// The very basic service to access stored data.
	/// </summary>
	public interface IDataService
	{
	}
}
