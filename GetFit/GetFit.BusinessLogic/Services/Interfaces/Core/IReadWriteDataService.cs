﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Services.Interfaces.Core
{
	public interface IReadWriteDataService<TModel, TEntity> : IWriteDataService<TModel>, IReadDataService<TModel, TEntity>
		 where TModel : class, IBaseModel, new()
		 where TEntity : class, IBaseEntity, new()
	{
	}
}
