﻿using System;
using System.Linq.Expressions;
using GetFit.BusinessLogic.Services.CRUD.Core;
using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Services.Interfaces.Core
{
	/// <summary>
	/// Contract for services for reading data from Database.
	/// </summary>
	public interface IReadDataService<TModel, TEntity> : IReadDataService<TModel>
		where TModel : class, IBaseModel, new()
		where TEntity : class, IBaseEntity, new()
	{
		/// <summary>
		/// Get one record by predicate
		/// </summary>
		/// <param name="predicate">Predicate</param>
		/// <returns>model object</returns>
		SingleOperationResult<TModel> GetSingleRecord(Expression<Func<TEntity, bool>> predicate);

		/// <summary>
		/// Get all records by predicate
		/// </summary>
		/// <param name="predicate">Predicate</param>
		/// <returns>List of model objects</returns>
		CollectionOperationResult<TModel> GetRecords(Expression<Func<TEntity, bool>> predicate);
	}

	/// <summary>
	/// Contract for services for reading data from Database.
	/// </summary>
	public interface IReadDataService<TModel> : IDataService
		where TModel : class, IBaseModel, new()
	{
		/// <summary>
		/// Get one record by id
		/// </summary>
		/// <param name="id">Record id</param>
		/// <returns>model object</returns>
		SingleOperationResult<TModel> GetSingleRecord(int id);

		/// <summary>
		/// Get all records
		/// </summary>
		/// <returns>List of models</returns>
		CollectionOperationResult<TModel> GetRecords();
	}
}
