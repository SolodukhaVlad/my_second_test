﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.BusinessLogic.Services.CRUD.Core;

namespace GetFit.BusinessLogic.Services.Interfaces
{
	public interface ICodeTableService
	{
		CollectionOperationResult<ICodeTableModel> GetRecords();
		ICodeTableModel GetSingleByDesc(string desc);
	}
}
