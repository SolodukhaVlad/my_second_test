﻿using GetFit.BusinessLogic.Services.Interfaces;
using GetFit.BusinessLogic.Interfaces;

namespace GetFit.BusinessLogic.Services.Interfaces
{
	public interface ICodeTableServicesFactory
	{
		ICodeTableService GetCodeTableService(string serviceKey);

		ICodeTableModel GetCodeTableModel<TEnum>(TEnum value)
			where TEnum : struct;
	}
}
