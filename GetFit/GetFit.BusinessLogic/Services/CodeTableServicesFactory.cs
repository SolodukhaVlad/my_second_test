﻿using System;
using System.Collections.Generic;
using System.Linq;
using GetFit.BusinessLogic.Interfaces;
using GetFit.BusinessLogic.Services.CRUD;
using GetFit.BusinessLogic.Services.Interfaces;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Services
{
	public class CodeTableRegisterEntry
	{
		public CodeTableRegisterEntry(string tableName, string enumName)
		{
			TableName = tableName;
			EnumName = enumName;

		}
		public string TableName { get; set; }
		public string EnumName { get; set; }
	}

	/// <summary>
	/// Service for getting codetable services
	/// </summary>
	public class CodeTableServicesFactory : ICodeTableServicesFactory
	{
		#region Data Members

		private readonly IDbContextOptionsProvider _dbContextOptionsProvider;
		private readonly Dictionary<CodeTableRegisterEntry, ICodeTableService> _codeTableServiceRegister;

		#endregion

		#region Constructors

		/// <summary>
		/// Create CodeTableServicesFactory
		/// </summary>
		/// <param name="optionsProvider">Dbcontext options provider</param>
		public CodeTableServicesFactory(IDbContextOptionsProvider optionsProvider)
		{
			_dbContextOptionsProvider = optionsProvider;
			_codeTableServiceRegister = new Dictionary<CodeTableRegisterEntry, ICodeTableService>();
			RegisterServices();
		}

		#endregion

		#region Private Methods

		private void RegisterServices()
		{
			//RegisterCodeTable<UserRole, UserRoles>();
		}

		private void RegisterCodeTable<TCodeTableEntity,TEnum>() 
			where TCodeTableEntity : class, ICodeTableEntity, new()
			where TEnum : struct 
		{
			_codeTableServiceRegister.Add(new CodeTableRegisterEntry(typeof(TCodeTableEntity).Name.ToLower(), typeof(TEnum).Name.ToLower()), 
				new CodeTableService<TCodeTableEntity>(_dbContextOptionsProvider));
		}

		private void RegisterCodeTable<TCodeTableEntity>()
			where TCodeTableEntity : class, ICodeTableEntity, new()
		{
			_codeTableServiceRegister.Add(new CodeTableRegisterEntry(typeof(TCodeTableEntity).Name.ToLower(), ""),
				new CodeTableService<TCodeTableEntity>(_dbContextOptionsProvider));
		}

		#endregion

		public ICodeTableModel GetCodeTableModel<TEnum>(TEnum value)
			where TEnum:struct
		{
			var service = _codeTableServiceRegister.FirstOrDefault(i => i.Key.EnumName == typeof(TEnum).Name.ToLower()).Value;
			return service.GetSingleByDesc(value.ToString());
		}

		#region ICodeTableServicesFactory Members

		public ICodeTableService GetCodeTableService(string serviceKey)
		{
			var key = serviceKey.ToLower();
			if (_codeTableServiceRegister.Any(i=>i.Key.TableName == key))
			{
				return _codeTableServiceRegister.FirstOrDefault(i=>i.Key.TableName == key).Value;
			}
			return null;
		}

		#endregion
	}

}
