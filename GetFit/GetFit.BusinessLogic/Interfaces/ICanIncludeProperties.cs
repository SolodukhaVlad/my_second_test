﻿using System.Linq;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Interfaces
{
	/// <summary>
	/// Contract for all models that can be created from entity that contains navigation properties
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public interface ICanIncludeProperties<TEntity> 
		where TEntity : IBaseEntity
	{
		/// <summary>
		/// Include navigation properties by specified query
		/// </summary>
		/// <param name="includeQuery">Query to include navigation properties</param>
		/// <returns>Query with included navigation properties</returns>
		IQueryable<TEntity> IncludeProperties(IQueryable<TEntity> includeQuery);
	}
}