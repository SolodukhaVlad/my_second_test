﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GetFit.BusinessLogic.Models
{
    public interface IUserModel
    {       
        [Required]
        string FirstName { get; set; }
        [Required]
        string LastName { get; set; }
        [Required]
        DateTime DayOfBirth { get; set; }
        [Required]
        string Email { get; set; }
        [Required]
        string Password { get; set; }

        string PhotoUrl { get; set; }

        int? AddressId { get; set; }
        int? ContactId { get; set; }        
    }
}