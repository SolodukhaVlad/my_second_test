﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Interfaces
{
	/// <summary>
	/// Contract for all classes that represent Models
	/// </summary>
	public interface IBaseModel : IHaveFriendlyName
	{
		/// <summary>
		/// Identification number for all models
		/// </summary>
		int Id { get; set; }

		/// <summary>
		/// Validates each property for BaseModel object.
		/// </summary>
		/// <param name="results">ICollection of ValidationResult objects to hold validation results.</param>
		/// <returns>True if the given entity is valid; otherwise, false.</returns>
		bool Validate(ICollection<ValidationResult> results);
	}

	public interface IBaseModel<TEntity> : ICanIncludeProperties<TEntity>, IBaseModel where TEntity : IBaseEntity
	{
		/// <summary>
		/// Convert entity to model
		/// </summary>
		/// <param name="entity">Entity to be converted</param>
		/// <returns>Converted model</returns>
		IBaseModel FromEntity(TEntity entity);

		/// <summary>
		/// Convert model to Entity
		/// </summary>
		/// <returns>Entity</returns>
		TEntity ToEntity();
	}
}
