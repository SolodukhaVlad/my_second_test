﻿using GetFit.BusinessLogic.Models;
using GetFit.DataAccess.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GetFit.BusinessLogic.Interfaces
{
    public interface IUserRegModel
    {
        ICollection<ValidationResult> Results { get; set; }

        IUserModel UserModel { get; set; }//
        AddressModel AddressModel { get; set; }//
        ContactModel ContactModel { get; set; }//

        bool FillEntityTiedModels();

        //ICollection<IBaseModel<BaseEntity>> EntityTiedModels { get; set; }

        //void FillModelsCollection();
    }
}
