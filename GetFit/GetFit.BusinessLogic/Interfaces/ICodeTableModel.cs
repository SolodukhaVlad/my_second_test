﻿
namespace GetFit.BusinessLogic.Interfaces
{
	/// <summary>
	/// Contract for all models that represent code tables
	/// </summary>
	public interface ICodeTableModel : IBaseModel
	{
		/// <summary>
		/// Get/Set Code value
		/// </summary>
		string Code { get; set; }

		/// <summary>
		/// Get/Set description value
		/// </summary>
		string Description { get; set; }
	}

}
