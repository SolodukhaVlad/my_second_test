﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Entities.Base;
using GetFit.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GetFit.BusinessLogic.Models
{
    public class TrainerRegModel : BaseModel, IUserRegModel
    {
        /// <summary>
        /// This property has been added for recording error messages if they occur while validation.
        /// We can pass it to UI to display the messages
        /// </summary>
        public ICollection<ValidationResult> Results { get; set; }

        //public ICollection<IBaseModel<BaseEntity>> EntityTiedModels { get; set; }

        public IUserModel UserModel { get; set; }
        public AddressModel AddressModel { get; set; }
        public ContactModel ContactModel { get; set; }

        public bool FillEntityTiedModels()
        {
            UserModel = new TrainerModel
            {
                FirstName = FirstName,
                LastName = LastName,
                DayOfBirth = DayOfBirth,
                Email = Email,
                Password = Password,
                PhotoUrl = PhotoUrl
            };

            if (StreetName != null && HouseNumber != null && City != null)
            {
                AddressModel = new AddressModel
                {
                    StreetName = StreetName,
                    HouseNumber = HouseNumber,
                    City = City
                };
            }

            if (PhoneNumber != null)
            {
                ContactModel = new ContactModel { PhoneNumber = PhoneNumber };
            }

            return true;//use "email-exist validation"
        }

        
        /// <summary>
        /// Necessary fields
        /// </summary>
        [Required(ErrorMessage = "Ім'я - обов'язкове поле")]
        [StringLength(100)]
        public string FirstName { get; set; }//goes to "Trainer" entity 
        [Required(ErrorMessage = "Прізвище - обов'язкове поле")]
        [StringLength(100)]
        public string LastName { get; set; }//goes to "Trainer" entity
        [Required(ErrorMessage = "Дата народження - обов'язкове поле")]
        [DataType(DataType.Date)]
        public DateTime DayOfBirth { get; set; }//goes to "Trainer" entity
        [Required(ErrorMessage = "Email - обов'язкове поле")]
        [EmailAddress(ErrorMessage = "Не коректний email")]
        public string Email { get; set; }//goes to "Trainer" entity
        [Required(ErrorMessage = "Пароль - обов'язкове поле")]
        [DataType(DataType.Password)]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "Довжина паролю має становити 6-12 символів")]
        public string Password { get; set; }//goes to "Trainer" entity

        /// <summary>
        /// Unnecessary fields
        /// </summary>
        [Url]
        [StringLength(200)]
        public string PhotoUrl { get; set; }//goes to "Trainer" entity
        [Phone]
        [StringLength(13)]
        public string PhoneNumber { get; set; }//goes to "Contacts" entity

        //Address is unnecessary. But if we decide to fill it, then all fields should be filled.
        [StringLength(50)]
        public string StreetName { get; set; }//goes to "Address" entity
        [StringLength(10)]
        public string HouseNumber { get; set; }//goes to "Address" entity
        [StringLength(50)]
        public string City { get; set; }//goes to "Address" entity

        public override bool Validate(ICollection<ValidationResult> results = null)
        {
            //object of class ValidationContext contains criteria of validation according to attributes specified to properties above
            var ctx = new ValidationContext(this, null, null);
            bool isValidated = Validator.TryValidateObject(this, ctx, results, true);
            Results = results;
            return isValidated;
        }
    }
}
