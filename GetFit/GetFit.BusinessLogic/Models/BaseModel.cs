﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Models
{
	/// <summary>
	/// Base class for all models
	/// </summary>
	public class BaseModel : IBaseModel
	{
		public int Id { get; set; }

		public virtual bool Validate(ICollection<ValidationResult> results = null)
		{
			var ctx = new ValidationContext(this, null, null);
			return Validator.TryValidateObject(this, ctx, results, true);
		}

		#region IBaseModel Members

		public virtual string GetFriendlyName()
		{
			return $"Record Id={Id}";
		}

		#endregion
	}

	/// <summary>
	/// Base class for models that are strongly tied to entity
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public abstract class BaseModel<TEntity> : BaseModel, IBaseModel<TEntity>
		where TEntity : IBaseEntity, new()
	{
		protected BaseModel()
		{
		}

		protected BaseModel(TEntity entity)
		{
			GetDataFromEntity(entity);
		}

		private void GetDataFromEntity(TEntity entity)
		{
			FromEntity(entity);
		}

		#region IBaseModel members

		public virtual IBaseModel FromEntity(TEntity entity)
		{
			Id = entity.Id;
			return this;
		}

		public virtual TEntity ToEntity()
		{
			return new TEntity { Id = Id };
		}

		public virtual IQueryable<TEntity> IncludeProperties(IQueryable<TEntity> includeQuery)
		{
			return includeQuery;
		}

		#endregion
	}
}
