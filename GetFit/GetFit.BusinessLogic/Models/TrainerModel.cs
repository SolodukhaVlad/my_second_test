﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Entities;
using GetFit.BusinessLogic.Models.Helper;
using System.ComponentModel.DataAnnotations;

namespace GetFit.BusinessLogic.Models
{
    public class TrainerModel : BaseModel<Trainer>, IUserModel
    {
        #region Model Data

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public DateTime DayOfBirth { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

        public string PhotoUrl { get; set; }

        public int? AddressId { get; set; }
        public int? ContactId { get; set; }

        public virtual AddressModel AddressModel { get; set; }
        public virtual ContactModel ContactModel { get; set; }

        public string Overview { get; set; }
        public int? GalleryId { get; set; }

        public virtual GalleryModel GalleryModel { get; set; }

        public ICollection<TrainingModel> Trainings { get; set; }
        public ICollection<TrainersSkillsModel> TrainersSkills { get; set; }

        #endregion

        #region Constructors
        public TrainerModel()
        {
            Trainings = new HashSet<TrainingModel>();
            TrainersSkills = new HashSet<TrainersSkillsModel>();
        }
        #endregion


        #region Overrides

        public override string GetFriendlyName() => $"Trainer {FirstName} {LastName}";

        public override IQueryable<Trainer> IncludeProperties(IQueryable<Trainer> includeQuery)
        {
            return includeQuery.Include(i => i.FirstName).Include(i => i.LastName);
        }

        public override IBaseModel FromEntity(Trainer entity)
        {
            Id = entity.Id;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            DayOfBirth = entity.DayOfBirth;
            Email = entity.Email;
            Password = entity.Password;
            PhotoUrl = entity.PhotoUrl;
            AddressId = entity.AddressId;
            ContactId = entity.ContactId;
            Overview = entity.Overview;
            GalleryId = entity.GalleryId;
            AddressModel = entity.Address.ToModel<AddressModel, Address>();
            ContactModel = entity.Contact.ToModel<ContactModel, Contact>();
            GalleryModel = entity.Gallery.ToModel<GalleryModel, Gallery>();


            Trainings = entity.Trainings.ToModels<TrainingModel, Training>().ToList();
            TrainersSkills = entity.TrainersSkills.ToModels<TrainersSkillsModel, TrainersSkills>().ToList(); ;

            return this;
        }

        public override Trainer ToEntity()
        {
            return new Trainer
            {
                Id = this.Id,
                FirstName = this.FirstName,
                LastName = this.LastName,
                DayOfBirth = this.DayOfBirth,
                Email = this.Email,
                Password = this.Password,
                PhotoUrl = this.PhotoUrl,
                AddressId = this.AddressId,
                ContactId = this.ContactId,
                Address = this.AddressModel.ToEntity(),
                Contact = this.ContactModel.ToEntity(),
                Overview = this.Overview,
                GalleryId = this.GalleryId,
                Gallery = this.GalleryModel.ToEntity(),

                TrainersSkills = this.TrainersSkills.Select(i => i.ToEntity()).ToList(),
                Trainings = this.Trainings.Select(i => i.ToEntity()).ToList()
            };
        }

        #endregion
    }
}
