﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;
using GetFit.BusinessLogic.Models.Helper;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace GetFit.BusinessLogic.Models
{
    public class AddressModel : BaseModel<Address>
    {
        #region Constructors
        public AddressModel()
        {
            Trainers = new HashSet<TrainerModel>();
            Clients = new HashSet<ClientModel>();
            Gyms = new HashSet<GymModel>();
        }
        #endregion

        #region Model Data
        [Required]
        public string StreetName { get; set; }
        [Required]
        public string HouseNumber { get; set; }
        [Required]
        public string City { get; set; }

        public ICollection<TrainerModel> Trainers { get; set; }
        public ICollection<ClientModel> Clients { get; set; }
        public ICollection<GymModel> Gyms { get; set; }
        #endregion

        #region Overrides

        public override string GetFriendlyName() => $"Address {StreetName} {HouseNumber} {City}";

        public override IQueryable<Address> IncludeProperties(IQueryable<Address> includeQuery)
        {
            return includeQuery.Include(i => i.City).Include(i => i.StreetName).Include(i => i.HouseNumber);
        }

        public override IBaseModel FromEntity(Address entity)
        {
            Id = entity.Id;
            StreetName = entity.StreetName;
            HouseNumber = entity.HouseNumber;
            City = entity.City;
            Trainers = entity.Trainers.ToModels<TrainerModel, Trainer>().ToList();
            Clients = entity.Clients.ToModels<ClientModel, Client>().ToList();
            Gyms = entity.Gyms.ToModels<GymModel, Gym>().ToList();

            return this;
        }

        public override Address ToEntity()
        {
            return new Address
            {
                Id = this.Id,
                StreetName = this.StreetName,
                HouseNumber = this.HouseNumber,
                City = this.City,
                Trainers = this.Trainers.Select(i => i.ToEntity()).ToList(),
                Clients = this.Clients.Select(i => i.ToEntity()).ToList(),
                Gyms = this.Gyms.Select(i => i.ToEntity()).ToList()
            };
        }

        #endregion
    }
}
