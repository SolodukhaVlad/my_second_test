﻿using GetFit.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using GetFit.BusinessLogic.Interfaces;
using GetFit.BusinessLogic.Models.Helper;

namespace GetFit.BusinessLogic.Models
{
    public class ClientModel : BaseModel<Client>, IUserModel
    {
        #region ModelData
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public DateTime DayOfBirth { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

        public string PhotoUrl { get; set; }

        public int? AddressId { get; set; }
        public int? ContactId { get; set; }

        public virtual AddressModel AddressModel { get; set; }
        public virtual ContactModel ContactModel { get; set; }

        public int GoalId { get; set; }

        public virtual GoalModel GoalModel { get; set; }

        public ICollection<ScheduleModel> Schedules { get; set; }
        public ICollection<ClientMetricsModel> ClientMetrics { get; set; }

        #endregion

        #region Constructors
        public ClientModel()
        {
            Schedules = new HashSet<ScheduleModel>();
            ClientMetrics = new HashSet<ClientMetricsModel>();
        }
        #endregion

        #region Overrides

        public override string GetFriendlyName() => $"Trainer {FirstName} {LastName}";

        public override IQueryable<Client> IncludeProperties(IQueryable<Client> includeQuery)
        {
            return includeQuery.Include(i => i.FirstName).Include(i => i.LastName);
        }

        public override IBaseModel FromEntity(Client entity)
        {
            Id = entity.Id;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            DayOfBirth = entity.DayOfBirth;
            Email = entity.Email;
            Password = entity.Password;
            PhotoUrl = entity.PhotoUrl;
            AddressId = entity.AddressId;
            ContactId = entity.ContactId;
            GoalId = entity.GoalId;
            AddressModel = entity.Address.ToModel<AddressModel, Address>();
            ContactModel = entity.Contact.ToModel<ContactModel, Contact>();
            GoalModel = entity.Goal.ToModel<GoalModel, Goal>();


            Schedules = entity.Schedules.ToModels<ScheduleModel, Schedule>().ToList();
            ClientMetrics = entity.ClientMetrics.ToModels<ClientMetricsModel, ClientMetrics>().ToList(); 

            return this;
        }

        public override Client ToEntity()
        {
            return new Client
            {
                Id = this.Id,
                FirstName = this.FirstName,
                LastName = this.LastName,
                DayOfBirth = this.DayOfBirth,
                Email = this.Email,
                Password = this.Password,
                PhotoUrl = this.PhotoUrl,
                AddressId = this.AddressId,
                ContactId = this.ContactId,
                Address = this.AddressModel.ToEntity(),
                Contact = this.ContactModel.ToEntity(),
                GoalId = this.GoalId,
                Goal = this.GoalModel.ToEntity(),

                Schedules = this.Schedules.Select(i => i.ToEntity()).ToList(),
                ClientMetrics = this.ClientMetrics.Select(i => i.ToEntity()).ToList()
            };
        }

        #endregion
    }
}
