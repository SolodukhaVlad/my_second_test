﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;
using GetFit.BusinessLogic.Models.Helper;
using Microsoft.EntityFrameworkCore;

namespace GetFit.BusinessLogic.Models
{
    public class ContactModel : BaseModel<Contact>
    {
        #region Constructors
        public ContactModel()
        {
            Trainers = new HashSet<TrainerModel>();
            Clients = new HashSet<ClientModel>();
            Gyms = new HashSet<GymModel>();
        }
        #endregion

        #region Model Data
        public string PhoneNumber { get; set; }
        public string FacebookUrl { get; set; }
        public string InstagramUrl { get; set; }

        public ICollection<TrainerModel> Trainers { get; set; }
        public ICollection<ClientModel> Clients { get; set; }
        public ICollection<GymModel> Gyms { get; set; }
        #endregion

        #region Overrides

        public override string GetFriendlyName() => $"Contact {PhoneNumber} {FacebookUrl} {InstagramUrl}";

        public override IQueryable<Contact> IncludeProperties(IQueryable<Contact> includeQuery)
        {
            return includeQuery.Include(i => i.PhoneNumber).Include(i => i.FacebookUrl).Include(i => i.InstagramUrl);
        }

        public override IBaseModel FromEntity(Contact entity)
        {
            Id = entity.Id;
            PhoneNumber = entity.PhoneNumber;
            FacebookUrl = entity.FacebookUrl;
            InstagramUrl = entity.InstagramUrl;
            Trainers = entity.Trainers.ToModels<TrainerModel, Trainer>().ToList();
            Clients = entity.Clients.ToModels<ClientModel, Client>().ToList();
            Gyms = entity.Gyms.ToModels<GymModel, Gym>().ToList();

            return this;
        }

        public override Contact ToEntity()
        {
            return new Contact
            {
                Id = this.Id,
                PhoneNumber = this.PhoneNumber,
                FacebookUrl = this.FacebookUrl,
                InstagramUrl = this.InstagramUrl,
                Trainers = this.Trainers.Select(i => i.ToEntity()).ToList(),
                Clients = this.Clients.Select(i => i.ToEntity()).ToList(),
                Gyms = this.Gyms.Select(i => i.ToEntity()).ToList()
            };
        }

        #endregion

    }
}
