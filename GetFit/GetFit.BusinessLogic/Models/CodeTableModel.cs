﻿using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Models
{
	/// <summary>
	/// Generic model for all codetables
	/// </summary>
	/// <typeparam name="TEntity">Entity type to create code table model</typeparam>
	public class CodeTableModel<TEntity> : BaseModel<TEntity>, ICodeTableModel
		where TEntity : class, ICodeTableEntity, new()
	{
		public CodeTableModel()
		{
		}

		public CodeTableModel(TEntity entity)
		{
			FromEntity(entity);
		}

		#region Overrides

		public override IBaseModel FromEntity(TEntity entity)
		{
			Id = entity.Id;
			Code = entity.Code;
			Description = entity.Description;
			return this;
		}

		public override TEntity ToEntity()
		{
			var entity = base.ToEntity();
			entity.Code = Code;
			entity.Description = Description;
			return entity;
		}

		#endregion

		#region ICodeTableModel Members

		public string Code { get; set; }
		public string Description { get; set; }

		#endregion
	}

}
