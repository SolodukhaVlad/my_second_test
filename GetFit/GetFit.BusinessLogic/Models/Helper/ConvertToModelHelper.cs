﻿using System.Collections.Generic;
using System.Linq;
using GetFit.BusinessLogic.Interfaces;
using GetFit.DataAccess.Interfaces;

namespace GetFit.BusinessLogic.Models.Helper
{
	/// <summary>
	/// Helper class for converting generic entities into generic models
	/// </summary>
	public static class ConvertToModelHelper
	{
		/// <summary>
		/// Converts collection of entities into collection of models
		/// </summary>
		public static IEnumerable<TModel> ToModels<TModel, TEntity>(this IEnumerable<TEntity> source)
			where TModel : class, IBaseModel<TEntity>, new()
			where TEntity : class, IBaseEntity, new()
		{

			return source.Select(entity => entity.ToModel<TModel, TEntity>());
		}
		/// <summary>
		/// Converts single entity into model
		/// </summary>
		public static TModel ToModel<TModel, TEntity>(this TEntity source)
			where TModel : class, IBaseModel<TEntity>, new()
			where TEntity : class, IBaseEntity, new()
		{
			var model = new TModel();
			return (TModel)model.FromEntity(source);
		}
	}
}
