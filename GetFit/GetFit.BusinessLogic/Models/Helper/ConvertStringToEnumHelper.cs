﻿using System;

namespace GetFit.BusinessLogic.Models.Helper
{
	/// <summary>
	/// Helper class for converting string into enum
	/// </summary>
	public static class ConvertStringToEnumHelper
	{
		/// <summary>
		/// Converts string into enum, if fails set default value
		/// </summary>
		/// <typeparam name="T">Type of enum</typeparam>
		/// <param name="value">String representation of enum</param>
		/// <param name="defaultValue">Value which should be set if converting fails</param>
		/// <returns></returns>
		public static T ToEnum<T>(this string value, T defaultValue) where T:struct, IConvertible
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException("T must be an enumerated type");
			}
			if (string.IsNullOrEmpty(value))
			{
				return defaultValue;
			}

			return Enum.TryParse<T>(value, true, out T result) ? result : defaultValue;
		}
	}
}
