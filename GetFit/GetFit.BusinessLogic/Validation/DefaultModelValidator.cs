﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GetFit.BusinessLogic.Validation
{
	/// <summary>
	/// Default class that validates model
	/// </summary>
	public class DefaultModelValidator<TModel> : IModelValidator<TModel>
			where TModel : class, new()
	{
		/// <summary>
		/// Method that validates model
		/// </summary>
		/// <param name="model">Model for validation</param>
		/// <returns>Errors occured during validation</returns>
		public virtual IEnumerable<ValidationResult> Validate(TModel model)
		{
			var errors = new List<ValidationResult>();
			if (model == null)
			{
				errors.Add(new ValidationResult("Model should not be null"));
			}
			return errors;
		}
	}
}
