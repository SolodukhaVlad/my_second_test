﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace GetFit.DataAccess.ContextOptionsProvider
{
	/// <summary>
	/// Factory to create dbcontext to perform migrations
	/// </summary>
	public class MigrationDbContextFactory : IDesignTimeDbContextFactory<GetFitContext>
	{
		public GetFitContext CreateDbContext(string[] args)
		{
			var builder = new DbContextOptionsBuilder<GetFitContext>();
			builder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=GetFitTestDB;Trusted_Connection=True;");
			return new GetFitContext(builder.Options);
		}
	}
}
