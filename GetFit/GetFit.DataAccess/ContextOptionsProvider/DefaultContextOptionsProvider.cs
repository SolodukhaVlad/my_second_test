﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using GetFit.DataAccess.Interfaces;

namespace GetFit.DataAccess.ContextOptionsProvider
{
	/// <summary>
	/// Class that provides default db context options
	/// </summary>
	public class DefaultContextOptionsProvider : IDbContextOptionsProvider
	{
		#region Constants

		private const string USE_MICROSOFT_SQL_LOCAL_DB_SETTING_KEY = "AppConfig:UseMicrosoftSQLLocalDb";
		private const string CONNECTION_STRING_KEY = "DefaultConnection";

		#endregion

		#region Data Members

		private readonly IConfigurationRoot _config;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates default context options provider and set config object
		/// </summary>
		/// <param name="config">Config object whichs is used to obtain connection string</param>
		public DefaultContextOptionsProvider(IConfigurationRoot config)
		{
			_config = config;
		}

		#endregion

		#region IDbContextOptionsProvider Members

		public DbContextOptions GetOptions()
		{
			var builder = new DbContextOptionsBuilder<GetFitContext>();
			var connectionString = _config.GetConnectionString(CONNECTION_STRING_KEY);

			if(bool.TryParse(_config[USE_MICROSOFT_SQL_LOCAL_DB_SETTING_KEY], out var UseMicrosoftSQLLocalDb))
			{
				if (UseMicrosoftSQLLocalDb) { builder.UseSqlServer(connectionString); }
				else { builder.UseInMemoryDatabase(connectionString); AllowMigration = false; }
			}

			builder.EnableSensitiveDataLogging();
			var options = builder.Options;
			return options;
		}

		public bool AllowMigration { get; private set; } = true;

		public bool AddDefaultData { get; } = true;

		#endregion
	}
}
