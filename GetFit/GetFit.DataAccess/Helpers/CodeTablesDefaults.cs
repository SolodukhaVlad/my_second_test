﻿using GetFit.DataAccess.Entities;
using System.Collections.Generic;
using GetFit.DataAccess.Interfaces;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Helpers
{
	public class CodeTablesDefaults
	{
		private readonly IDbContextOptionsProvider _optionsProvider;

		public CodeTablesDefaults(IDbContextOptionsProvider optionsProvider)
		{
			_optionsProvider = optionsProvider;
		}

		public void FillAll()
		{
			FillGoals();
			FillMetrics();
			FillSkills();
			FillTrainingStatus();
			FillTrainingType();
			FillZoneTypes();
		}
		
		private void FillGoals()
		{
			using (var context = new GetFitContext(_optionsProvider.GetOptions()))
			{
				var listOfGoals = new List<Goal>
				{
					new Goal{Code = "Зменшення ваги", Description = "Програма для зменшення ваги тіла"},
					new Goal{Code = "Збільшення ваги", Description = "Програма для збільшення ваги тіла"},
					new Goal{Code = "Тонус", Description = "Підвищення загального тонусу м'язів"},
					new Goal{Code = "Гнучкість", Description = "Покрщення гнучкості тіла"},
					new Goal{Code = "Збільшення сили м'язів", Description = "Програма для збільшення сили м'язів"},
					new Goal{Code = "Аеробіка", Description = "Комплексна програма аеробних вправ"},
					new Goal{Code = "Покращення витривалості", Description = "Програма для покращення витривалості тіла"},
					new Goal{Code = "Бодібілдинг", Description = "Комплексна програма для бодібілдингу"}                    
				};
				
				context.Goals.AddRange(listOfGoals);
				context.SaveChanges();
			}
		}

		private void FillMetrics()
		{
			using (var context = new GetFitContext(_optionsProvider.GetOptions()))
			{
				var listOfMetrics = new List<Metric>
				{
					new Metric{Code = "Стать", Description = ""},
					new Metric{Code = "Зріст", Description = "Зріст у сантиметрах"},
					new Metric{Code = "Вага", Description = "Вага у кілограмах"}
				};

				context.Metrics.AddRange(listOfMetrics);
				context.SaveChanges();
			}
		}

		private void FillSkills()
		{
			using (var context = new GetFitContext(_optionsProvider.GetOptions()))
			{
				var listOfSkills = new List<Skill>
				{
					new Skill{Code = "Кардіотренування", Description = ""},
					new Skill{Code = "Бодібілдинг", Description = ""},
					new Skill{Code = "Збільшення витривалості", Description = ""},
					new Skill{Code = "М'язова сила", Description = ""},
					new Skill{Code = "Зменшення ваги", Description = ""},
					new Skill{Code = "TRX", Description = ""},
					new Skill{Code = "Спортивний табір", Description = ""},
					new Skill{Code = "Йога", Description = ""},
					new Skill{Code = "Бокс", Description = ""},
					new Skill{Code = "Боротьба", Description = ""},
					new Skill{Code = "Плавання", Description = ""},
				};

				context.Skills.AddRange(listOfSkills);
				context.SaveChanges();
			}
		}

		private void FillTrainingStatus()
		{
			using (var context = new GetFitContext(_optionsProvider.GetOptions()))
			{
				var listOfStatuses = new List<TrainingStatus>
				{
					new TrainingStatus{Code = "Заплановно", Description = ""},
					new TrainingStatus{Code = "Виконано", Description = ""},
					new TrainingStatus{Code = "Відмінено", Description = ""},
					new TrainingStatus{Code = "Перенесено", Description = ""},
				};

				context.TrainingStatuses.AddRange(listOfStatuses);
				context.SaveChanges();
			}
		}

		private void FillTrainingType()
		{
			using (var context = new GetFitContext(_optionsProvider.GetOptions()))
			{
				var listOfTrTypes = new List<TrainingType>
				{
					new TrainingType{Code = "Аеробні вправи", Description = ""},
					new TrainingType{Code = "Силові вправи", Description = ""},
					new TrainingType{Code = "Группове заняття", Description = ""},
					new TrainingType{Code = "Спортивний майданчик", Description = ""},
					new TrainingType{Code = "Спаринг", Description = ""},
					new TrainingType{Code = "Плавання", Description = ""},
					new TrainingType{Code = "Спортивний табір", Description = ""},
				};

				context.TrainingTypes.AddRange(listOfTrTypes);
				context.SaveChanges();
			}
		}

		private void FillZoneTypes()
		{
			using (var context = new GetFitContext(_optionsProvider.GetOptions()))
			{
				var listOfZoneTypes = new List<ZoneType>
				{                   
					new ZoneType{Code = "Тренажерний зал", Description = ""},
					new ZoneType{Code = "Аеробний зал", Description = ""},
					new ZoneType{Code = "Зал для групових занять", Description = ""},
					new ZoneType{Code = "Ринг", Description = ""},
					new ZoneType{Code = "Басейн", Description = ""},
					new ZoneType{Code = "Спортивний майданчик", Description = ""},
					new ZoneType{Code = "Стадіон", Description = ""},
				};
				context.ZoneTypes.AddRange(listOfZoneTypes);
				context.SaveChanges();
			}
		}
	}
}
