﻿using System.Linq;
using GetFit.DataAccess.Entities;
using GetFit.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GetFit.DataAccess
{
	public class GetFitContext : DbContext
	{
		#region Constructors

		public GetFitContext(IDbContextOptionsProvider optionsProvider)
			: base(optionsProvider.GetOptions())
		{
			if (optionsProvider.AllowMigration)
			{
				this.Database.Migrate();
			}
		}

		public GetFitContext(DbContextOptions options)
			: base(options)
		{
		}

		#endregion

		#region DbSets

		public DbSet<Trainer>  Trainers { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Gym> Gyms { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<TrainersSkills> TrainersSkills { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Metric> Metrics { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<TrainingStatus> TrainingStatuses { get; set; }
        public DbSet<TrainingType> TrainingTypes { get; set; }        
        public DbSet<ZoneType> ZoneTypes { get; set; }

		#endregion

		#region Overrides

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
            //Specify your connection string to deploy the database on your machine
			optionsBuilder.UseSqlServer("Data Source = ADMIN-PC; Initial Catalog = FitnessTrainersDB; Integrated Security = True");
		}

		protected override void OnModelCreating(ModelBuilder modelbuilder)
		{
			foreach (var relationship in modelbuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
			{
				relationship.DeleteBehavior = DeleteBehavior.Restrict;
			}

			base.OnModelCreating(modelbuilder);
		}

		#endregion
	}
}
