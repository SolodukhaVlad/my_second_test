﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GetFit.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using GetFit.DataAccess.Helpers;

namespace GetFit.DataAccess.Repository
{
	/// <summary>
	/// DataRepository class, represents basic functionality to access database.
	/// </summary>
	public class DataRepository<TEntity> : IDataRepository<TEntity>
		where TEntity : class, IBaseEntity
	{
		#region Data Members

		private readonly IDbContextOptionsProvider _optionsProvider;
		private static readonly Dictionary<Type, DateTime> lastChangesDatesMap;

		#endregion

		#region Constructors

		static DataRepository()
		{
			lastChangesDatesMap = new Dictionary<Type, DateTime>();
		}

		/// <summary>
		/// Creates DataRepository to work with TEntity entities 
		/// </summary>
		/// <param name="optionsProvider">Data base context options provider</param>
		public DataRepository(IDbContextOptionsProvider optionsProvider)
		{
			_optionsProvider = optionsProvider;

			if (optionsProvider.AddDefaultData && lastChangesDatesMap.Count == 0)
			{
				new CodeTablesDefaults(optionsProvider).FillAll();
			}

			if (!lastChangesDatesMap.ContainsKey(typeof(TEntity)))
			{
				lastChangesDatesMap.Add(typeof(TEntity), DateTime.Now);
			}
		}

		#endregion

		#region Private Methods

		private void UpdateLastChangesDate()
		{
			lastChangesDatesMap[typeof(TEntity)] = DateTime.Now;
		}

		#endregion

		#region IDataRepository Members

		public virtual TEntity Add(TEntity entity)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				var result = context.Set<TEntity>().Add(entity);
				context.SaveChanges();
				UpdateLastChangesDate();
				return result.Entity;
			}
		}

		public virtual void AddRange(IEnumerable<TEntity> entities)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				foreach (var entity in entities)
				{
					context.Set<TEntity>().Add(entity);
				}
				context.SaveChanges();
				UpdateLastChangesDate();
			}
		}

		public virtual void Remove(object id)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				var entity = context.Set<TEntity>().Find(id);
				context.Set<TEntity>().Remove(entity);
				context.SaveChanges();
				UpdateLastChangesDate();
			}
		}

		public virtual void Remove(TEntity entity)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				context.Set<TEntity>().Attach(entity);
				context.Set<TEntity>().Remove(entity);
				context.SaveChanges();
				UpdateLastChangesDate();
			}
		}

		public virtual TEntity Update(TEntity newEntity)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				var result = context.Set<TEntity>().Attach(newEntity);
				context.Update(newEntity);
				context.SaveChanges();
				UpdateLastChangesDate();
				return result.Entity;
			}
		}

		public virtual void Update(IEnumerable<TEntity> newEntity)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				context.Set<TEntity>().AttachRange(newEntity);
				context.UpdateRange(newEntity);
				context.SaveChanges();
				UpdateLastChangesDate();
			}
		}

		public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null,
			Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				IQueryable<TEntity> query = context.Set<TEntity>();
				if (asNoTracking)
				{
					query = query.AsNoTracking();
				}
				if (predicate != null)
					query = query.Where(predicate);
				if (includeFunction != null)
					query = includeFunction(query);
				return query.ToList();
			}
		}

		public virtual TEntity GetSingle(Expression<Func<TEntity, bool>> predicate = null,
			Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false)
		{
			return Get(predicate, includeFunction, asNoTracking).FirstOrDefault();
		}

		public DateTime GetSyncData()
		{
			return lastChangesDatesMap[typeof(TEntity)];
		}

		public bool Any(Expression<Func<TEntity, bool>> predicate = null)
		{
			using (var context = new GetFitContext(_optionsProvider))
			{
				IQueryable<TEntity> query = context.Set<TEntity>();

				if (predicate != null)
				{
					return query.Any(predicate);
				}
				return query.Any();
			}
		}

		#endregion
	}
}
