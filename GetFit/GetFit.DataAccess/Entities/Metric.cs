﻿using System;
using System.Collections.Generic;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Metric : BaseCodeTableEntity
    {
        public Metric()
        {
            ClientMetrics = new HashSet<ClientMetrics>();
        }

        public ICollection<ClientMetrics> ClientMetrics { get; set; }
    }
}
