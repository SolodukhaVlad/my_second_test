﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class ClientMetrics : BaseEntity
    {       
        [ForeignKey("Client")]
        public int ClientId { get; set; }
        [ForeignKey("Metric")]
        public int MetricId { get; set; }
        public string Value { get; set; }

        public virtual Client Client { get; set; }
        public virtual Metric Metric { get; set; }
    }
}
