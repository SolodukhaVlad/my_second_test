﻿using System;
using System.Collections.Generic;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class TrainingStatus : BaseCodeTableEntity
    {
        public TrainingStatus()
        {
            Trainings = new HashSet<Training>();
        }

        public ICollection<Training> Trainings { get; set; }
    }
}
