﻿using GetFit.DataAccess.Interfaces;

namespace GetFit.DataAccess.Entities.Base
{
	public abstract class BaseCodeTableEntity : BaseEntity, ICodeTableEntity
	{
		public string Code { get; set; }
		public string Description { get; set; }
	}
}
