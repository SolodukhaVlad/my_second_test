﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Zone : BaseEntity
    {       
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [ForeignKey("ZoneType")]
        public int ZoneTypeId { get; set; }
        [ForeignKey("Gym")]
        public int GymId { get; set; }

        public virtual ZoneType ZoneType { get; set; }
        public virtual Gym Gym { get; set; }

    }
}
