﻿using System;
using System.Collections.Generic;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Skill : BaseCodeTableEntity
    {
        public Skill()
        {
            TrainersSkills = new HashSet<TrainersSkills>();
        }

        public ICollection<TrainersSkills> TrainersSkills { get; set; }
    }
}
