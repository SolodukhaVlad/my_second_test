﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Contact : BaseEntity
    {
        public Contact()
        {
            Trainers = new HashSet<Trainer>();
            Clients = new HashSet<Client>();
            Gyms = new HashSet<Gym>();
        }
        
        [StringLength(13)]
        public string PhoneNumber { get; set; }
        [StringLength(100)]
        public string FacebookUrl { get; set; }
        [StringLength(100)]
        public string InstagramUrl { get; set; }

        public ICollection<Trainer> Trainers { get; set; }
        public ICollection<Client> Clients { get; set; }
        public ICollection<Gym> Gyms { get; set; }
    }
}
