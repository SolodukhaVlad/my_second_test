﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class TrainersSkills : BaseEntity
    {       
        [ForeignKey("Trainer")]
        public int TrainerId { get; set; }
        [ForeignKey("Skill")]
        public int SkillId { get; set; }

        public virtual Trainer Trainer { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
