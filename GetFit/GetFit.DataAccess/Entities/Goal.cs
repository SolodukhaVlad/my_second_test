﻿using System;
using System.Collections.Generic;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Goal : BaseCodeTableEntity
    {
        public Goal()
        {
            Clients = new HashSet<Client>();
        }

        public ICollection<Client> Clients { get; set; }
    }
}
