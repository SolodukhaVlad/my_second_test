﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Schedule : BaseEntity
    {
        public Schedule()
        {
            Trainings = new HashSet<Training>();
        }
        [ForeignKey("Client")]
        public int ClientId { get; set; }        
        [StringLength(500)]
        public string Description { get; set; }

        public virtual Client Client { get; set; }
        public ICollection<Training> Trainings { get; set; }
    }
}
