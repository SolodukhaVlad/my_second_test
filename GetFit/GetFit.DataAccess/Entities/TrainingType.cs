﻿using System;
using System.Collections.Generic;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class TrainingType : BaseCodeTableEntity
    {
        public TrainingType()
        {
            Trainings = new HashSet<Training>();
        }

        public ICollection<Training> Trainings { get; set; }
    }
}
