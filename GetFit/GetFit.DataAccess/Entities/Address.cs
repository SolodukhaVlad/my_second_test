﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Address : BaseEntity
    {
        public Address()
        {
            Trainers = new HashSet<Trainer>();
            Clients = new HashSet<Client>();
            Gyms = new HashSet<Gym>();
        }
                
        [Required]
        [StringLength(50)]
        public string StreetName { get; set; }
        [Required]
        [StringLength(10)]
        public string HouseNumber { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        
        public ICollection<Trainer> Trainers { get; set; }
        public ICollection<Client> Clients { get; set; }
        public ICollection<Gym> Gyms { get; set; }
    }
}
