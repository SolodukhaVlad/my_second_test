﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Photo : BaseEntity
    {        
        [StringLength(200)]
        public string PhotoUrl { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
        [ForeignKey("Gallery")]
        public int GaleryId { get; set; }

        public virtual Gallery Gallery { get; set; }
    }
}
