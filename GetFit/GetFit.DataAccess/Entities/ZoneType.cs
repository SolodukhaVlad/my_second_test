﻿using System;
using System.Collections.Generic;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class ZoneType : BaseCodeTableEntity
    {
        public ZoneType()
        {
            Zones = new HashSet<Zone>();
        }

        public ICollection<Zone> Zones { get; set; }
    }
}
