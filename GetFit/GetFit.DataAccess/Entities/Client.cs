﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Client : User
    {
        public Client()
        {
            Schedules = new HashSet<Schedule>();
            ClientMetrics = new HashSet<ClientMetrics>();
        }

        [ForeignKey("Goal")]
        public int GoalId { get; set; }

        public virtual Goal Goal { get; set; }

        public ICollection<Schedule> Schedules { get; set; }
        public ICollection<ClientMetrics> ClientMetrics { get; set; }
    }
}
