﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using GetFit.DataAccess.Entities.Base;

namespace GetFit.DataAccess.Entities
{
    public class Gallery : BaseEntity
    {
        public Gallery()
        {
            Trainers = new HashSet<Trainer>();
            Gyms = new HashSet<Gym>();
            Photos = new HashSet<Photo>();
        }
        
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(500)]
        public string Description { get; set; }

        public ICollection<Trainer> Trainers { get; set; }
        public ICollection<Gym> Gyms { get; set; }
        public ICollection<Photo> Photos { get; set; }
    }
}
