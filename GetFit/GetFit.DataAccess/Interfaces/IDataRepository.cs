﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GetFit.DataAccess.Interfaces
{
	/// <summary>
	/// Base IDataAccessLayer interface
	/// </summary>
	public interface IDataRepository<TEntity> where TEntity : IBaseEntity
	{
		/// <summary>
		/// This is a basic generic method for adding a new TEntity object to the database where TEntity : BaseEntity.
		/// </summary>
		/// <param name="entity">Entity object with generic type TEntity where TEntity : BaseEntity.</param>
		TEntity Add(TEntity entity);

		/// <summary>
		/// This is a basic generic method for adding a list of new TEntities to the database where TEntity : BaseEntity.
		/// </summary>
		/// <param name="entities">List of entities object with generic type TEntity where TEntity : BaseEntity.</param>
		void AddRange(IEnumerable<TEntity> entities);

		/// <summary>
		/// This is a basic generic method for phisical removing records from the database by id.
		/// </summary>
		/// <param name="id">Identification number of record in database.</param>
		void Remove(object id);

		/// <summary>
		/// This is a basic generic method for phisical removing record from the database 
		/// by specified type of entity
		/// </summary>
		/// <param name="entity">Entity object with generic type TEntity where TEntity : class.</param>
		void Remove(TEntity entity);

		/// <summary>
		/// This is a basic generic method for update record in the database by the given TEntity object where TEntity : BaseEntity.
		/// </summary>
		/// <param name="entity">TEntity object where TEntity : BaseEntity.</param>
		TEntity Update(TEntity entity);

		/// <summary>
		/// This is a basic generic method for update list of records in the database by the given IEnumerable<TEntity> object where TEntity : BaseEntity.
		/// </summary>
		/// <param name="entity">TEntity object where TEntity : BaseEntity.</param>
		void Update(IEnumerable<TEntity> newEntity);

		/// <summary>
		/// This is a basic generic method for get list of entities from the database 
		/// by specified type of entity, lambda expresion and expresion of navigation property for include.
		/// </summary>
		/// <param name="predicate">Expression for choice data from data base.</param>
		/// <param name="includeFunction">Function for choice list of included properties.</param>
		/// <param name="asNoTracking">Flag indicating if AsNoTracking is turned on or off.</param>
		/// <returns>Collection of entities by the specified predicate with included properties.</returns>
		IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false);

		/// <summary>
		/// Try find any object by predicate in database
		/// </summary>
		/// <param name="predicate">Expression for choice data from data base.</param>
		/// <returns>True if object is exist</returns>
		bool Any(Expression<Func<TEntity, bool>> predicate = null);

		/// <summary>
		/// This is a basic generic method for get only one entity from the database 
		/// by specified type of entity, lambda expresion and expresion of navigation property for include.
		/// </summary>
		/// <param name="predicate">Expression for choice data from data base.</param>
		/// <param name="includeFunction">Function for choice list of included properties.</param>
		/// <param name="asNoTracking">Flag indicating if AsNoTracking is turned on or off.</param>
		/// <returns>One entity object by the specified predicate with included properties.</returns>
		TEntity GetSingle(Expression<Func<TEntity, bool>> predicate = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeFunction = null, bool asNoTracking = false);

		/// <summary>
		/// Returns last date when current table has been updated
		/// </summary>
		/// <returns>last changes date</returns>
		DateTime GetSyncData();
	}
}
