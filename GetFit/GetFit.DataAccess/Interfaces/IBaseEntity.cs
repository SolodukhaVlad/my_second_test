﻿namespace GetFit.DataAccess.Interfaces
{
	public interface IBaseEntity
	{
		int Id { get; set; }
	}
}
