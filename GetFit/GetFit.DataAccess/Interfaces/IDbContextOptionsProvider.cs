﻿using Microsoft.EntityFrameworkCore;

namespace GetFit.DataAccess.Interfaces
{
	/// <summary>
	/// Contract for all classes which are providing db context options
	/// </summary>
	public interface IDbContextOptionsProvider
	{
		/// <summary>
		/// Returns options for database context
		/// </summary>
		/// <returns></returns>
		DbContextOptions GetOptions();

		/// <summary>
		/// True if migration is allowed
		/// </summary>
		bool AllowMigration { get; }

		/// <summary>
		/// True if default data should be added into db
		/// </summary>
		bool AddDefaultData { get; }
	}
}
