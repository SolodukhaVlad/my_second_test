﻿namespace GetFit.DataAccess.Interfaces
{
	public interface ICodeTableEntity : IBaseEntity
	{
		string Code { get; set; }
		string Description { get; set; }
	}
}
